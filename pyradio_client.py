#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import subprocess
from socket import *
import curses
import sys


client_socket = socket(AF_INET, SOCK_STREAM)

class Client:

    IP = 'raspberrypi'
    PORT = 12121
    volume = 80
    VOLUME_STEP = 5
    canale = 0
    BUFSIZE = 4096
    data = ''
    channelCount = 0


    # ritorno il canale corrente datomi il testo ritornato da getInfo()
    def getChannel(self, text):
        canale = text.split('#')[1].split('/')[0]
        return canale

    def getChannelCount():
        return self.channelCount


    # eseguo i comandi che devo dare appena accesa la radio
    def firstCommands(self):

        # prende il numero totale di radio che ci sono registrate
        client_socket.send(b"getNumRadio")
        self.channelCount = int(client_socket.recv(self.BUFSIZE))
        
        # prendo il volume corrente
        client_socket.send(b"getVolume")
        s = str(client_socket.recv(self.BUFSIZE))
        print(">>>>>>>>>>>>>>>" +s)
        self.volume = int(s[9:12])

        # prendo il canale corrente
        client_socket.send(b"getInfos")
        try:
            infos = str(client_socket.recv(self.BUFSIZE))
        except:
            self.canale = int(self.getChannel(infos))
        




    # comunico con il server data
    def communicate(self, data):
        if (data != 'Q' and data != 'q'):
            if data == 'volume_plus':
                client_socket.send(b"getVolume")
#                self.volume = int(str(client_socket.recv(self.BUFSIZE))[9:12])
                tmp = str(client_socket.recv(self.BUFSIZE))
                print(">>>>>>>>>>>>> " +tmp)
                self.volume = int(tmp[9:12])
                if int(self.volume) < 100:
                    self.volume = self.volume + self.VOLUME_STEP
                    data = "mpc volume " + str(self.volume)
                else:
                    data = None
            elif data == 'volume_minus': 
                client_socket.send(b"getVolume")
#                self.volume = int(str(client_socket.recv(self.BUFSIZE))[9:12])
                tmp = str(client_socket.recv(self.BUFSIZE))
                print(">>>>>>>>>>>>> " +tmp)
                self.volume = int(tmp[9:12])
                if int(self.volume) > 0:
                    self.volume = self.volume - self.VOLUME_STEP
                    data = "mpc volume " + str(self.volume)
                else:
                    data = None
            elif data == 'channel_plus':
                if self.canale < self.channelCount:
                    self.canale += 1
                    data = "mpc play " + str(self.canale)
                else:
                    data = None
            elif data == 'channel_minus':
                if self.canale > 1:
                    self.canale -= 1
                    data = "mpc play " + str(self.canale)
                else:
                    data = None
            elif data == "getInfos":
                client_socket.send(b"getInfos")
                infos = str(client_socket.recv(self.BUFSIZE))
                self.canale = self.getChannel(infos)
            elif data == "getChannelCount":
                return self.channelCount
                data = None
            else:
                data = None
            

            if data is not None:
                client_socket.send(bytes(data.encode()))
                data = client_socket.recv(self.BUFSIZE)
                #print("Ricevuto: " + data)
            

        else:
            data = "q"
            client_socket.send(bytes(data.encode()))
            data = client_socket.recv(self.BUFSIZE)
            client_socket.close()
            


    def __init__(self):
        client_socket.connect((self.IP, self.PORT))
        self.firstCommands()