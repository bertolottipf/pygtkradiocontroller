#!/usr/bin/env python
# -*- coding: iso-8859-15 -*- 

__author__ = "BERTOLOTTI Paolo Francesco"


import gui
import sys


if __name__ == "__main__":
    try:
        gui.PyGtkRadio()
    except KeyboardInterrupt:
        data = "q"
        gui.client.communicate(data)
        sys.exit()
