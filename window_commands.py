from gi.repository import GObject
from gi.repository import Gtk
import gui

class Window_Commands(Gtk.Window):

    volumeLabel = ""
    volumeCombo = None
    stationLabel = ""
    stationCombo = None

    ''' Cambia canale '''
    def on_channelCombo_changed(self, channelCombo):
        pass
        

    ''' Cambia volume '''
    def on_volumeCombo_changed(self, volumeCombo):
        print(str(100-volumeCombo.get_active()*5))
        gui.client.communicate("mpc volume " + str(100-volumeCombo.get_active()*5))
        pass


    ''' Chiude la finestra ''' 
    def on_close_clicked(self, button):
        self.destroy()


    def __init__(self, nCh):


        Gtk.Window.__init__(self, title="PyRadio")

        self.vbox = Gtk.Box(spacing=6)
        self.vbox.set_orientation(Gtk.Orientation.VERTICAL)
        self.add(self.vbox)
        self.vbox.set_homogeneous(False)


        self.hbox1 = Gtk.Box(spacing=6)
        self.hbox1.set_homogeneous(False)
        self.hbox1.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.hbox1.set_homogeneous(False)

        self.vbox.pack_start(self.hbox1, True, True, 0)




        self.volumeLabel = Gtk.Label("Volume")
        self.hbox1.pack_start(self.volumeLabel, True, True, 0)


        volumeComboEl = Gtk.ListStore(int, str)
        for i in range(0, 105):
            if i % 5 == 0:
                volumeComboEl.append([100-i, str(100-i)])

        self.volumeCombo = Gtk.ComboBox.new_with_model_and_entry(volumeComboEl)
        self.volumeCombo.connect("changed", self.on_volumeCombo_changed)
        self.volumeCombo.set_entry_text_column(1)

        self.hbox1.pack_start(self.volumeCombo, True, True, 0)






        self.hbox2 = Gtk.Box(spacing=6)
        self.hbox2.set_homogeneous(False)
        self.hbox2.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.hbox2.set_homogeneous(False)

        self.vbox.pack_start(self.hbox2, True, True, 0)

        self.StationLabel = Gtk.Label("Station")
        self.hbox2.pack_start(self.StationLabel, True, True, 0)


        chCount = int(nCh)
        print(chCount)
        channelComboEl = Gtk.ListStore(int, str)
        for i in range(0, chCount):
            channelComboEl.append([chCount-i, str(chCount-i)])

        self.channelCombo = Gtk.ComboBox.new_with_model_and_entry(channelComboEl)
        #self.channelCombo.connect("changed", self.on_channelCombo_change)
        self.channelCombo.set_entry_text_column(1)

        self.hbox2.pack_start(self.channelCombo, True, True, 0)







        self.button2 = Gtk.Button(label="Goodbye")
        self.button2.connect("clicked", self.on_close_clicked)
        self.vbox.pack_start(self.button2, True, True, 0)



        self.show_all()