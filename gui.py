#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gi.repository import GObject
from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator
import os
import sys
import pyradio_client
import about
import window_commands
import random



try:
    client = pyradio_client.Client()
except KeyboardInterrupt:
    data = "q"
    gui.client.communicate(data)
    gui.client.close()
    sys.exit()


class PyGtkRadio:

    def getInfos(self):
        infos = client.communicate('getInfos')
        return infos

    def window_commands(self, w, buf):
        #chCount = "3" #TODO
        chCount = client.communicate('getChannelCount')
        winc = window_commands.Window_Commands(chCount)

    def about_window(sels, w, buf):
        win = about.About()

    def channel_plus(self, w, buf):
        client.communicate('channel_plus')

    def channel_minus(self, w, buf):
        client.communicate('channel_minus')

    def volume_plus(self, w, buf):
        client.communicate('volume_plus')

    def volume_minus(self, w, buf):
        client.communicate('volume_minus')

    def exit(self, w, buf):
        client.communicate('q')
        sys.exit()


    def __init__(self):

        pwd = os.getcwd()
        
        icon_image = os.path.dirname(__file__) + "/icon.png"
        if not os.path.isfile(icon_image):
            icon_image = "/usr/share/unity/icons/panel-shadow.png"

        ind = appindicator.Indicator.new(
            "radio", 
            icon_image, 
            appindicator.IndicatorCategory.APPLICATION_STATUS)
        ind.set_status(appindicator.IndicatorStatus.ACTIVE)

        # create a menu
        menu = gtk.Menu()

        # create some
        menu_el = ['Channel ►', 'Channel ◄', 'Volume ▲', 'Volume ▼', 'WinCommands', 'About', 'Exit']








        buf = menu_el[0] # channel+
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.channel_plus, buf)
        menu_items.show()






        buf = menu_el[1] # channel-
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.channel_minus, buf)
        menu_items.show()

        buf = menu_el[2] # volume+
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.volume_plus, buf)
        menu_items.show()

        buf = menu_el[3] # volume-
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.volume_minus, buf)
        menu_items.show()

        buf = menu_el[4] # WinCommands
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.window_commands, buf)
        menu_items.show()

        buf = menu_el[5] # about
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.about_window, buf)
        menu_items.show()


        buf = menu_el[6] #exit
        menu_items = gtk.MenuItem(buf)
        menu.append(menu_items)
        menu_items.connect("activate", self.exit, buf)
        menu_items.show()


        

        ind.set_menu(menu)

        gtk.main()