from gi.repository import GObject
from gi.repository import Gtk

class About(Gtk.Dialog):

    image = 1
    buttonClose = 2
    label = 3



    def on_close_clicked(self, button):
        self.destroy()


    def __init__(self):

        # creo la finestra intitolandola e dando dimensioni
        Gtk.Dialog.__init__(self, title="About")
        
        self.resize(200, 200)

        box = self.get_content_area()
        box.set_orientation(Gtk.Orientation.VERTICAL)

        # creo l'immagine
        self.image = Gtk.Image()
        self.image.set_from_file("about.png")

        box.add(self.image)



        # the scrolledwindow
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_border_width(5)
        scrolled_window.set_size_request(200,105)
        # there is always the scrollbar (otherwise: AUTOMATIC - only if needed
        # - or NEVER)
        scrolled_window.set_policy(
            Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        # creo la label e la aggiungo
        self.label = Gtk.Label()
        self.label.set_text("Questo programma ti permette di pilotare a distanza la tua PyRadio.\n\
            Per informazioni su come costruirla, leggi il file README.md\n\
            Programma distribuito dalla BPFKing.com e creato da BERTOLOTTI Paolo Francesco")
        self.label.set_line_wrap(True)

        scrolled_window.add_with_viewport(self.label)


        box.add(scrolled_window)
        



        # creo il bottone e lo aggiungo
        self.buttonClose = Gtk.Button(label="Close")
        self.buttonClose.connect("clicked", self.on_close_clicked)

        box.add(self.buttonClose)


        



        self.add(scrolled_window)

        self.show_all()


    